import click
import logging
from pathlib import Path
from datetime import datetime
from getpass import getpass
from zipatopy import ZipatoPy as Client


logging.basicConfig(level=logging.INFO)

LOG = logging.getLogger("zipato_api_automation")


@click.command()
@click.option("-v", "--verbose", is_flag=True)
@click.option("-u", "--user", default="bard@smartbuild.hu", help="Username of the executor")
@click.option("-a", "--adduser", help="Username of the user to be added")
@click.option("-i", "--id", help="User Id of new user")
@click.option("-b", "--boxid", help="BOX Id of new user")
def main(verbose, user, adduser, id, boxid):
    passwd = getpass(f"Please provide {user}'s password:\n")
    zcl = Client(user, passwd, verbose=verbose)
    available_systems = zcl.get_systems()
    LOG.info(f"Listing systems available for {user}...")
    for system in available_systems:
        LOG.info(f"APT: {system['name']}\tID: {system['uuid']}")
        if adduser:
            roleid = zcl.get_system_role_id("TENANT", system["uuid"])
            user_body = {
                "id": 157701 if adduser == "support@yabune-home.hu" else id,
                "username": adduser,
                "boxUser": "6a612332-a561-46ec-8acf-55bc4f73405f" if adduser == "support@yabune-home.hu" else boxid,
                "role": roleid if roleid else ""
            }
            zcl.add_user_to_system(system["uuid"], user_body)


if __name__ == "__main__":
    main()
